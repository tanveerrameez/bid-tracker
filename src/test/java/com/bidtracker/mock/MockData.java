package com.bidtracker.mock;

import com.auctionbidtracker.model.BidItem;
import com.auctionbidtracker.model.BidUser;

public class MockData {
	
	private static BidUser user1=new BidUser(1,"Tom Bentley");
	private static BidUser user2=new BidUser(2,"John Williams");
	
	private static BidItem bidItem1=new BidItem(1, "Audi Q5");
	private static BidItem bidItem2=new BidItem(2, "BMW 3 Series"); 
	
	public static BidUser getBidUser1() {
		return user1;
	}
	
	public static BidUser getBidUser2() {
		return user2;
	}
	public static BidUser[] getBidUsers() {
		return new BidUser[] {user1, user2};
	}

	public static BidItem getBidItem1() {
		return bidItem1;
	}

	public static BidItem getBidItem2() {
		return bidItem2;
	}
}

package com.bidtracker.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.auctionbidtracker.exception.InvalidBidException;
import com.auctionbidtracker.model.Bid;
import com.auctionbidtracker.model.BidItem;
import com.auctionbidtracker.model.BidUser;
import com.auctionbidtracker.service.AuctionBidTracker;
import com.auctionbidtracker.service.AuctionBidTrackerImpl;
import com.bidtracker.mock.MockData;

public class AuctionBidTrackerlmplTest {
	
	private AuctionBidTracker auctionBidTracker;

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		auctionBidTracker=new AuctionBidTrackerImpl();
	}

	@Test
	public void testValidFirstUserBid() throws InvalidBidException {
		BidUser bidUser = MockData.getBidUser1();
		BidItem bidItem=MockData.getBidItem1();
		Bid bid=new Bid(bidUser.getId(), 100, bidItem);
		auctionBidTracker.recordUsersBidOnItem(bid);
		Optional<Bid> optionalBid = auctionBidTracker.getCurrentWinningBidForItem(bidItem);
		
		assertTrue(optionalBid.isPresent());
		assertEquals(bid.getValue(), optionalBid.get().getValue());
	}
	
	@Test
	public void testValidSunsequentUserBid() throws InvalidBidException {
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem=MockData.getBidItem1();
		Bid bid=new Bid(bidUser1.getId(), 100, bidItem);
		auctionBidTracker.recordUsersBidOnItem(bid);
		
		BidUser bidUser2 = MockData.getBidUser2();
		Bid bid2=new Bid(bidUser2.getId(), 101, bidItem);
		auctionBidTracker.recordUsersBidOnItem(bid2);
		Optional<Bid> optionalBid = auctionBidTracker.getCurrentWinningBidForItem(bidItem);
		
		assertTrue(optionalBid.isPresent());
		assertEquals(bid2.getValue(), optionalBid.get().getValue());
	}

	@Test
	public void testLowerSunsequentUserBidShouldThrowException() throws InvalidBidException {
		thrown.expect(InvalidBidException.class);
		thrown.expectMessage("The bid of £99 on bid item Audi Q5 is too low. It should be more than the current winning bid: £100");
	    
		BidUser bidUser = MockData.getBidUser1();
		BidItem bidItem=MockData.getBidItem1();
		Bid bid=new Bid(bidUser.getId(), 100, bidItem);
		auctionBidTracker.recordUsersBidOnItem(bid);
		
		Bid bid2=new Bid(bidUser.getId(), 99, bidItem);
		auctionBidTracker.recordUsersBidOnItem(bid2);
	}
	
	@Test
	public void testWithNegativeUserBidShouldThrowException() throws InvalidBidException {
		thrown.expect(InvalidBidException.class);
		thrown.expectMessage("Bid value must be positive");
	    
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem1=MockData.getBidItem1();
		Bid bid=new Bid(bidUser1.getId(), -1, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid);

	}
	
	@Test
	public void testWithZeroValueUserBidShouldThrowException() throws InvalidBidException {
		thrown.expect(InvalidBidException.class);
		thrown.expectMessage("Bid value must be positive");
	    
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem1=MockData.getBidItem1();
		Bid bid=new Bid(bidUser1.getId(), 0, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid);

	}
	
	
	@Test
	public void testGetAllItemsBidByUser() throws InvalidBidException {
		
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem1=MockData.getBidItem1();
		Bid bid=new Bid(bidUser1.getId(), 100, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid);
		
		BidItem bidItem2=MockData.getBidItem2();
		Bid bid2=new Bid(bidUser1.getId(), 101, bidItem2);
		auctionBidTracker.recordUsersBidOnItem(bid2);
		
		Set<BidItem> bidItemSet = auctionBidTracker.getAllItemsBidByUser(bidUser1.getId());
		assertNotNull(bidItemSet);
		assertFalse(bidItemSet.isEmpty());
		assertEquals(2, bidItemSet.size());
		assertTrue(bidItemSet.contains(bidItem1));
		assertTrue(bidItemSet.contains(bidItem2));
	}
	
	@Test
	public void testGetAllBidsForABidItem() throws InvalidBidException {
		
		BidItem bidItem1=MockData.getBidItem1();
		BidUser bidUser1 = MockData.getBidUser1();
		
		Bid bid1=new Bid(bidUser1.getId(), 100, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid1);
		
		BidUser bidUser2 = MockData.getBidUser1();
		Bid bid2=new Bid(bidUser2.getId(), 101, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid2);
		
		
		List<Bid> bidList = auctionBidTracker.getAllBidsForItem(bidItem1);
		assertNotNull(bidList);
		assertFalse(bidList.isEmpty());
		assertEquals(2, bidList.size());
		assertTrue(bidList.contains(bid1));
		assertTrue(bidList.contains(bid2));
	}
	
	
	@Test
	public void testGetAllBidsForAInvalidBidItem() throws InvalidBidException {
		
		thrown.expect(InvalidBidException.class);
		thrown.expectMessage("BidItem id must be positive");
		
		BidItem bidItem1=new BidItem(-1,"Test Bid Item");
		BidUser bidUser1 = MockData.getBidUser1();
		Bid bid1=new Bid(bidUser1.getId(), 100, bidItem1);
		auctionBidTracker.recordUsersBidOnItem(bid1);
	}
	
	@Test
	  public void getCurrentWinningBidForItemWithoutAnyBid() {
		BidItem bidItem1=MockData.getBidItem1();
	    Optional<Bid> bid = auctionBidTracker.getCurrentWinningBidForItem(bidItem1);
	    assertEquals(Optional.empty(), bid);
	  }
	
	@Test
	  public void recordUserBidOnItemForMultyipleUsersWithMultipleBids() throws InvalidBidException {
		
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem1=MockData.getBidItem1();
		BidUser bidUser2 = MockData.getBidUser2();
		
		auctionBidTracker.recordUsersBidOnItem(new Bid(bidUser1.getId(), 100, bidItem1));
		auctionBidTracker.recordUsersBidOnItem(new Bid(bidUser2.getId(), 101, bidItem1));
		auctionBidTracker.recordUsersBidOnItem(new Bid(bidUser1.getId(), 102, bidItem1));

	    List<Bid> actualBidsListOnItem1 = auctionBidTracker.getAllBidsForItem(bidItem1);
	    List<Bid> expectedBidsListOnItem1 = Arrays.asList(
	        new Bid(bidUser1.getId(), 100, bidItem1),
	        new Bid(bidUser2.getId(), 101, bidItem1),
	        new Bid(bidUser1.getId(), 102, bidItem1));
	    assertEquals(expectedBidsListOnItem1, actualBidsListOnItem1);
	  }

	
	@Test
	  public void recordUserBidOnItems_inAMultithreadedEnvironment() {
		
		BidUser bidUser1 = MockData.getBidUser1();
		BidItem bidItem1=MockData.getBidItem1();
	    AtomicInteger invalidBidsCount = new AtomicInteger(0);

	    // Make 10000 bids on 4 different threads.
	    int totalNbBids = 10000;
	    ExecutorService executor = Executors.newFixedThreadPool(4);
	    IntStream.range(0, totalNbBids).forEach(
	        i -> executor.submit(
	            () -> {
	              try {
	            	  auctionBidTracker.recordUsersBidOnItem(new Bid(bidUser1.getId(), i, bidItem1));
	              } catch(InvalidBidException e) {
	                invalidBidsCount.incrementAndGet();
	              }
	            }
	        )
	    );
	    shutDownExecutor(executor);

	    List<Bid> actualBidsMade = auctionBidTracker.getAllBidsForItem(bidItem1);

	    // asserting that all bids were processed
	    assertEquals(totalNbBids, actualBidsMade.size() + invalidBidsCount.get());
	    // asserting that the accepted bids for the item are all ordered by increasing value
	    assertEquals(actualBidsMade, sortBidListByValue(actualBidsMade));
	    // asserting that the last bid is for 9999
	    Bid lastBidMade = actualBidsMade.get(actualBidsMade.size() - 1);
	    assertEquals(totalNbBids - 1, lastBidMade.getValue());
	  }
	
	private List<Bid> sortBidListByValue(List<Bid> bidList) {
	    return bidList.stream()
	        .sorted(Comparator.comparing(Bid::getValue))
	        .collect(Collectors.toList());
	  }


	  private void shutDownExecutor(ExecutorService executor) {
	    try {
	     executor.awaitTermination(20, TimeUnit.SECONDS);
	    } catch (InterruptedException e) {
	      System.err.println("tasks interrupted");
	    } finally {
	      executor.shutdownNow();
	    }
	  }

}

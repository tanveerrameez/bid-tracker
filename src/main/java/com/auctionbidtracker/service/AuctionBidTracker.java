package com.auctionbidtracker.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.auctionbidtracker.exception.InvalidBidException;
import com.auctionbidtracker.model.Bid;
import com.auctionbidtracker.model.BidItem;

/**
 * The interface is for an Auction Bid Tracker.
 * 
 */
public interface AuctionBidTracker {

	/**
	   * Record a user's bid on a bidItem
	   * @param bid the bid to record
	   * @param bidItem the BidItem to bid on
	   * @throws InvalidBidValueException when the bid value is invalid
	   */
	  void recordUsersBidOnItem(Bid bid) throws InvalidBidException;
	  
	   /**
	   * @param bidItem
	   * @return the current winning bid for a given item
	   */
	  public Optional<Bid> getCurrentWinningBidForItem(BidItem bidItem);
	  
	  /**
	   * @param bidItem 
	   * @return the list of all bids made for the given BidItem
	   */
	  List<Bid> getAllBidsForItem(BidItem bidItem);
	  
	  /**
	   * @param user the user to get the list of items for
	   * @return the list of all bidItems bid on by the given user
	   */
	  Set<BidItem> getAllItemsBidByUser(int userId);
	  
}

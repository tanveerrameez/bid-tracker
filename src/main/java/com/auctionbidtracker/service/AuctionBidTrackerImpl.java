package com.auctionbidtracker.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.util.CollectionUtils;

import com.auctionbidtracker.exception.InvalidBidException;
import com.auctionbidtracker.model.Bid;
import com.auctionbidtracker.model.BidItem;

public class AuctionBidTrackerImpl implements AuctionBidTracker {

	
	private final Map<BidItem, List<Bid>>  auctionMap;
	
	public AuctionBidTrackerImpl() {
		auctionMap=new ConcurrentHashMap<>();
	}
	
	@Override
	public void recordUsersBidOnItem(@Valid Bid bid) throws InvalidBidException {
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Bid>> bidViolations = validator.validate(bid);
		if(!CollectionUtils.isEmpty(bidViolations)) {
			List<String> messages=new ArrayList<>();
			for (ConstraintViolation<Bid> violation : bidViolations) {
			    messages.add(violation.getMessage()); 
			}
			throw new InvalidBidException(messages);
		}
		Set<ConstraintViolation<BidItem>> bidItemViolations = validator.validate(bid.getBidItem());
		if(!CollectionUtils.isEmpty(bidItemViolations)) {
			List<String> messages=new ArrayList<>();
			for (ConstraintViolation<BidItem> violation : bidItemViolations) {
			    messages.add(violation.getMessage()); 
			}
			throw new InvalidBidException(messages);
		}
		Optional<BidItem> optionalBidItem=Optional.of(bid.getBidItem());
		if(bid!=null && optionalBidItem.isPresent())
			recordUserBidOnItem(bid);

	}
	
	// synchronized method to ensure that only one bid is processed at one time
	  private synchronized void recordUserBidOnItem(Bid bid) throws InvalidBidException {
	    checkBidIsHighEnough(bid);
	    addBidOnItem(bid);
	  }
      
	  private void checkBidIsHighEnough(Bid bid) throws InvalidBidException {
		    Optional<Bid> currentHighestBid = getCurrentWinningBidForItem(bid.getBidItem());
		    if(currentHighestBid.isPresent() && bid.getValue() <= currentHighestBid.get().getValue()) {
		      throw new InvalidBidException(String.format(
		          "The bid of £%s on bid item %s is too low. It should be more than the current winning bid: £%s",
		          bid.getValue(), bid.getBidItem().getName(), currentHighestBid.get().getValue()));
		    }
	  }
	  
	  @Override
	  public Optional<Bid> getCurrentWinningBidForItem(BidItem bidItem) {
	    List<Bid> bids = getAllBidsForItem(bidItem);
	    return bids.isEmpty() ? Optional.empty() : bids.stream().max((a,b)->a.getValue()-b.getValue());
	  }
	  
	private void addBidOnItem(Bid bid) {
       
	    List<Bid> bidsOnItem = auctionMap.getOrDefault(bid.getBidItem(), new ArrayList<>());
	    bidsOnItem.add(bid);
	    auctionMap.put(bid.getBidItem(), bidsOnItem);
	  }


	@Override
	public List<Bid> getAllBidsForItem(BidItem bidItem) {
         List<Bid> bidList=auctionMap.getOrDefault(bidItem, new ArrayList<>());

         if(!CollectionUtils.isEmpty(bidList))
	           return Collections.unmodifiableList(bidList);
         else return bidList;
	}

	@Override
	  public Set<BidItem> getAllItemsBidByUser(int userId) {
	    return Collections.unmodifiableSet(
	    		auctionMap.entrySet().stream()
	        .filter(entry -> containsBidFromUser(entry.getValue(), userId))
	        .map(Map.Entry::getKey).collect(Collectors.toSet()));
	  }
	
	private boolean containsBidFromUser(List<Bid> bidsList, int userId) {
	    return bidsList.stream().anyMatch(bid -> bid.isFromUser(userId));
	  }

}

package com.auctionbidtracker.model;

import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * The representation of a user of the auction
 */
public class BidUser {
	
 @Positive(message ="BidUser id must be positive")
 @NotNull(message ="BidUser id cannot be null")
 private final int id;
 
 @NotBlank(message ="BidUser name cannot be blank")
  private final String name;

  public BidUser(int id, String name) {
    this.id = id;
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("{ id: %s, name: %s }", getId(), name);
  }


  @Override
  public boolean equals(Object o) {
    boolean equals = false;
    if (o == this) {
      equals = true;
    } else if (o instanceof BidUser) {
      BidUser user = (BidUser) o;
      equals = Objects.equals(getId(), user.getId()) && Objects.equals(name, user.name);
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), name);
  }

public int getId() {
	return id;
}
}
package com.auctionbidtracker.model;

import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * The auction bid from a userId
 */
public class Bid {
	
  @Min(value=1, message ="Bid id must be positive")
  @NotNull(message ="Bid id cannot be null")
  private final int userId;
  
  @NotNull(message ="BidItem cannot be null")
  private final BidItem bidItem;
  
  @Min(value=1, message ="Bid value must be positive")
  @NotNull(message ="Bid value c")
  private final int bidValue;
  

  public Bid(int userId, int bidValue, BidItem bidItem) {
    this.userId = userId;
    this.bidValue = bidValue;
    this.bidItem=bidItem;
  }

  public int getUserId() {
    return userId;
  }

  public int getValue() {
    return bidValue;
  }

  public boolean isFromUser(int userId) {
    return this.userId==userId;
  }

  @Override
  public String toString() {
    return String.format("{ User: %s, bidValue: %s }", userId, bidValue);
  }

  @Override
  public boolean equals(Object o) {
    boolean equals = false;
    if (o == this) {
      equals = true;
    } else if (o instanceof Bid) {
      Bid bid = (Bid) o;
      equals = Objects.equals(userId, bid.userId) && bidValue == bid.bidValue && bidItem == bid.bidItem;
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, bidValue, bidItem);
  }

public BidItem getBidItem() {
	return bidItem;
}

}
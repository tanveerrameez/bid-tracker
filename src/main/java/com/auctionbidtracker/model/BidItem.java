package com.auctionbidtracker.model;

import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Min;


/**
 * The representation of an item of the auction
 */
public class BidItem {
	
	@Min(value=1, message ="BidItem id must be positive")
  private final int id;
  
  @NotBlank(message ="BidItem name cannot be empty")
  private final String name;


  public BidItem(Integer id, String name) {
    this.id = id;
    this.name = name;
   
  }

  @Override
  public String toString() {
    return String.format("{ id: %s, name: %s}", getId(), getName());
  }

  @Override
  public boolean equals(Object o) {
    boolean equals = false;
    if (o == this) {
      equals = true;
    } else if (o instanceof BidItem) {
      BidItem item = (BidItem) o;
      equals = Objects.equals(getId(), item.getId()) && Objects.equals(getName(), item.getName());
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getName());
  }

public int getId() {
	return id;
}

public String getName() {
	return name;
}
}

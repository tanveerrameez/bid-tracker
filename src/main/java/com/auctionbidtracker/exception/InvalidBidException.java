package com.auctionbidtracker.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * An exception thrown when the bid is invalid
 * How is the bid invalid is defined by business rules
 */
public class InvalidBidException extends Exception {

	private static final long serialVersionUID = 3728384250673458093L;
	
    private List<String> messages=new ArrayList<>();
    
    public InvalidBidException(String message) {
    super(message);
    }
    
    public InvalidBidException(List<String> messages) {
        this.messages=messages;
        
     }
    
    public String getMessage()
    {
    	if(super.getMessage()!=null)
    		return super.getMessage();
    	StringBuffer messageBuffer=new StringBuffer();
    	for(String message:messages) {
    		messageBuffer.append(message).append("\n");
    	}
    	return messageBuffer.toString();
    }
}
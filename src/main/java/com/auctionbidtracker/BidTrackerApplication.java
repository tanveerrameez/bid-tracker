package com.auctionbidtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BidTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BidTrackerApplication.class, args);
	}

}

